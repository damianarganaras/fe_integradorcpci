/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui core components
import { List, ListItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import {useState} from "react"
import axios from 'axios'

import styles from "assets/jss/material-kit-react/views/landingPage.js";

// Sections for this page
import ProductSection from "../LandingPage/Sections/ProductSection.js";
import TeamSection from "../LandingPage/Sections/TeamSection.js";
import WorkSection from "../LandingPage/Sections/WorkSection.js";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

const clickEnElBoton = () =>{
    console.log("Se hizo click en el botón")

    axios ("http://localhost:6256/api/products")
    .then(lectura => {
      const datosLectura=lectura.data
      console.log(datosLectura)
      setListaComentarios(datosLectura)
    })
    .catch(error => {
      console.log(error)
    })
  }


function HomePage(props) {
    const classes = useStyles();
    const { ...rest } = props;
    return (
        <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="OnlineShop"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white",
          }}
          {...rest}
        />
        <Parallax filter image={require("assets/img/landing-bg.jpg").default}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <h1 className={classes.title}>Conseguí lo que quieras, acá!</h1>
                <h4>
                    Agarrate las cachas que esta pagina te volara el copete!
                    No hace absolutamente nada, pero qué manera de no hacer nada!
                    Hace clic en el siguiente botón para ver el vídeo antes de empezar:
                </h4>
                <br />
                <Button
                  color="danger"
                  size="lg"
                  href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fas fa-play" />
                  Mira el vídeo
                </Button>
                <input onClick={()=>clickEnElBoton()} type="button" value="♥"></input>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            <ProductSection />
          </div>
        </div>
        <Footer />
      </div>
    );
  }

  export default HomePage;
