/* eslint-disable prettier/prettier */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable prettier/prettier */

import {useState, useEffect} from "react"
import axios from "axios"
import ProductCard from "components/Products/ProductCard"
import GridContainer from "components/Grid/GridContainer.js";
import ProductosCard from "views/CartPage/News3CardDemo";
import GridItem from "components/Grid/GridItem.js";



function Products() {

  const url = "http://18.218.91.244/OnlineShopAPI/api/products";
  const [products, setProducts] = useState({
      loading: false,
      data: null,
      error: false
  })

  useEffect(() =>{
      setProducts({
          loading: true,
          data: null,
          error: false
        })
        axios.get(url)
        .then(response => {
            setProducts({
                loading: false,
                data: response.data,
                error: false
            })
        })
        .catch(()=>{
            setProducts({
                loading: false,
                data: null,
              error: true
            })
        })
    },[url])

    let content = null;

    if(products.error){
        content = <h1>
            Ha ocurrido un error, por favor recargue la página.
        </h1>
    }

    if(products.loading){
        content = <p>
            Cargando...
        </p>
    }

    if(products.data){
        content =
        products.data.map((product, key) =>
            <GridItem xs={4} key={key}>
                <ProductCard
                    product={product}
                />
            </GridItem>
        )
    }

  return (
      <div>
          <GridContainer spacing={10} justify="center">
                {content}
          </GridContainer>
          <ProductosCard></ProductosCard>
      </div>
  )
}

export default Products
