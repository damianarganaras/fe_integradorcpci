/* eslint-disable prettier/prettier */
/* eslint-disable no-undef */
/* eslint-disable prettier/prettier */
/* eslint-disable react/prop-types */
/* eslint-disable prettier/prettier */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom"
import CardMedia from '@material-ui/core/CardMedia'
import Favorite from '@material-ui/icons/Favorite';
import Button from 'components/CustomButtons/Button.js';
import Tooltip from "@material-ui/core/Tooltip";
import axios from 'axios';
import {useState, useEffect} from "react"


const useStyles = makeStyles({
    root: {
      maxWidth: 300,
      maxHeight: "100%",
    },
    media: {
      height: 140,
    },
  });

  export default function ProductCard(props) {

    const[cartitemid, setcartitemid] = useState(null);

    useEffect(() => {
      const cartitem = { cart_item_id: 1, cart_id:23, product_id:1, quantity:5 };
        axios.post(`http://18.218.91.244/OnlineShopAPI/api/cartitems`, {cartitem})
          .then(response => setcartitemid(response.data.id));
    },[]);
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
      <Tooltip
        id="tooltip-right"
        title={props.product.description}
        placement="right"
        classes={{ tooltip: classes.tooltip }}
      >
        <Typography className={classes.title} color="textSecondary" gutterBottom>
            <CardMedia
            className={classes.media}
            image={props.product.image}
            />
        </Typography>
      </Tooltip>
        <Typography variant="h6" component="h6">
            <Link to={`/products/${props.product.id}`}>
                        {props.product.title}
                    </Link>
        </Typography>
        <Typography className={classes.pos} color="primary">
            $ {props.product.price}
        </Typography>
        {/* <Typography variant="body2" component="p">
            {props.product.description}
          <br />
          {'"a benevolent smile"'}
        </Typography> */}
      </CardContent>
      <CardActions>
        <Button justify="center" color="warning" round><Favorite /> Agregar al Carrito {cartitemid}</Button>
      </CardActions>
    </Card>
  );
}
